# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]

SUMMARY="A parser and an API designed to allow applications to support the OFX banking standard"
DESCRIPTION="
LibOFX is a generic library to allow financial software to easily support the
Open Financial eXchange specification. Two utilities are currently included
with LibOFX: ofxdump and ofxtoqif. ofxdump writes to stdout, in human readable
form, everything the library understands about a particular OFX response file.
ofxtoqif is an OFX response to the QIF (Quicken Interchange Format) file
converter. Much OFX information is lost since the QIF file format is very
primitive, but ofx2qif should allow importation of bank statements in any
software that support QIF until said software supports the library directly.
LibOFX was implemented from the full OFX 1.6 spec and comes with a developers
manual.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc
    ofxconnect [[ description = [ Build a exemplarily utility to test OFX Direct Connect ] ]] "

DEPENDENCIES="
    build:
        sys-apps/help2man
        doc? ( app-doc/doxygen[dot] )
    build+run:
        app-text/opensp
        ofxconnect? (
            dev-cpp/libxml++:2.6[>=2.6]
            net-misc/curl[>=7.9.7]
        )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc doxygen'
    'doc dot'
 )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'ofxconnect libcurl' )

DEFAULT_SRC_INSTALL_PARAMS=( docdir=/usr/share/doc/${PNVR} )
DEFAULT_SRC_COMPILE_PARAMS=( "${DEFAULT_SRC_INSTALL_PARAMS[@]}" )

src_install() {
    default

    if option doc ; then
        edo mv "${IMAGE}"/usr/share/doc/${PN}{,-${PVR}}/html
        edo rm -r "${IMAGE}"/usr/share/doc/${PN}
    fi
}

